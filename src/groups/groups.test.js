

const _ = require('lodash/collection');
const { expect } = require('chai');
const Groups = require('./');
const nock = require('nock');

describe('Groups', () => {
  it('should search groups by name', async () => {
    const result = await Groups.search('token', 'Theodor Bastard');

    expect(result).to.be.instanceOf(Array);
    expect(result.map(item => item.id)).to.include(121032);
  });

  it('should search groups by display_name or ID', async () => {
    const query = _.sample(['theodor_bastard', 121032]);
    const result = await Groups.search('token', query);

    expect(result).to.be.instanceOf(Array);
    expect(result[0].screen_name).to.be.equal('theodor_bastard');
  });

  it('should combine same results', async () => {
    const result = await Groups.search('token', 'Theodor Bastard');

    expect(result).to.be.instanceOf(Array);
    expect(result.length).to.equal(1);
  });
});

const by_name_stub = {
  response: {
    count: 1,
    items: [
      {
        id: 121032,
        name: 'THEODOR BASTARD',
        screen_name: 'theodor_bastard',
        is_closed: 0,
        type: 'group',
        is_admin: 0,
        is_member: 1,
        photo_50: 'https://pp.userap...be3/BZH_d7twVHE.jpg',
        photo_100: 'https://pp.userap...be2/GVyU_mWBRrU.jpg',
        photo_200: 'https://pp.userap...be1/zXYRoHfR-vs.jpg',
      },
    ],
  },
};


const by_display_name_stub = {
  response: [
    {
      id: 121032,
      name: 'THEODOR BASTARD',
      screen_name: 'theodor_bastard',
      is_closed: 0,
      type: 'group',
      is_admin: 0,
      is_member: 1,
      photo_50: 'https://pp.userap...be3/BZH_d7twVHE.jpg',
      photo_100: 'https://pp.userap...be2/GVyU_mWBRrU.jpg',
      photo_200: 'https://pp.userap...be1/zXYRoHfR-vs.jpg',
    },
  ],
};

nock('https://api.vk.com')
  .get('/method/groups.search')
  .query(q => q.access_token)
  .reply(200, by_name_stub)
  .persist();

nock('https://api.vk.com')
  .get('/method/groups.getById')
  .query(q => q.access_token)
  .reply(200, by_display_name_stub)
  .persist();
