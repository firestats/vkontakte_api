const config = require('../../config');
const _ = require('lodash/array');
const axios = require('axios');
const jsonp = require('jsonp');

/**
 * Поиск по группам.
 * - `Token (String)` - VK Access Token
 * - `Search Word (String)` - ID, короткое имя или полное имя группы
 *
 * Возвращает список групп, представленных в виде объекта [группы](https://vk.com/dev/objects/group).
 *
 * @param {String} token
 * @param {String} search_word
 * @returns {Array}
 */
async function search(token, search_word) {
  if (!token) throw new Error('Access token must be defined!');

  const search_by_name_url = `${config.api_host}/groups.search?q=${search_word}&access_token=${token}&v=5.69`;
  const search_by_display_name_url = `${config.api_host}/groups.getById?group_id=${search_word}&access_token=${token}&v=5.69`;

  let raw_network_results = {};

  try {
    if (typeof window === 'object') {
      raw_network_results = await Promise.all([
        jsonp(search_by_name_url),
        jsonp(search_by_display_name_url),
      ]);
    } else {
      raw_network_results = await Promise.all([
        axios.get(search_by_name_url),
        axios.get(search_by_display_name_url),
      ]);
    }
  } catch (error) {
    throw new Error(error);
  }

  const results = [];

  for (let i = 0, n = raw_network_results.length; i < n; i += 1) {
    const data_to_work_with = typeof window === 'object' ?
      raw_network_results[i] :
      raw_network_results[i].data;

    const { error, response } = data_to_work_with;

    // Error 100 means there is no results found,
    // ant we dont have to throw error on this case
    // So just continue

    // eslint-disable-next-line no-continue
    if (error && error.error_code === 100) continue;
    if (error) throw new Error(JSON.stringify(error));

    if (response.items) {
      results.push(response.items);
    } else if (typeof response === 'object') {
      results.push(response);
    }
  }

  const flatten = _.flatten(results);
  return _.uniqBy(flatten, 'id');
}

module.exports = search;
