const axios = require('axios');
const jsonp = require('jsonp');
const _ = require('lodash/collection');
const config = require('../../../config');

/**
 * Получение групп пользователя.
 *
 * **Лимит - 1000 групп!**
 *
 * - `access_token (String)` - VK Access Token
 * - `user_id (Number)` - ID пользователя
 * - `filter (Object)` - Объект вида `{ is_admin: Boolean }`
 *
 * Возвращает список групп, представленных в виде объекта [группы](https://vk.com/dev/objects/group).
 *
 * @param {String} access_token
 * @param {Number} user_id
 * @returns {Array}
 */
async function all(access_token, user_id, filter = {}) {
  if (!access_token) throw new Error('Access token must be defined!');

  let url = `${config.api_host}/groups.get?extended=1&user_id=${user_id}&access_token=${access_token}&v=5.69`;
  let groups = {};

  if (filter.is_admin) url += '&filter=admin';

  try {
    if (typeof window === 'object') {
      groups = await jsonp(url);
    } else {
      const groups_response = await axios.get(url);
      groups = groups_response.data;
    }
  } catch (error) {
    throw new Error(error);
  }

  const { error, response } = groups;
  if (error) throw new Error(JSON.stringify(error));

  return _.sortBy(response.items, [g => g.is_admin !== 1]);
}

module.exports = all;
