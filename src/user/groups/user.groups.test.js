
const _ = require('lodash/collection');
const { expect } = require('chai');
const UserGroups = require('./');
const nock = require('nock');

describe('User Groups', () => {
  it('Should get all groups', async () => {
    const groups = await UserGroups.all('token', 1);

    const sample_group = _.sample([28261334, 152624627, 24946565]);

    expect(groups).to.be.instanceof(Array);
    expect(groups.map(g => g.id)).to.include(sample_group);
  });

  it('User groups should be sorted by Admin Rights', async () => {
    const groups = await UserGroups.all('token', 1);

    expect(groups).to.be.instanceof(Array);
    expect(groups.map(g => g.is_admin)).to.eql([1, 1, 0]);
  });
});

nock('https://api.vk.com/')
  .get('/method/groups.get')
  .query(q => q.access_token)
  .reply(200, {
    response: {
      count: 105,
      items: [{
        id: 28261334,
        name: 'TJ',
        screen_name: 'tj',
        is_closed: 0,
        type: 'page',
        is_admin: 0,
        is_member: 0,
        photo_50: 'https://pp.userap...fe1/3rZwq-HbTX8.jpg',
        photo_100: 'https://pp.userap...fe0/EAu2JzSja3U.jpg',
        photo_200: 'https://pp.userap...fdf/SrU0DO9rGpQ.jpg',
      }, {
        id: 152624627,
        name: 'Гусь',
        screen_name: 'therealgus',
        is_closed: 0,
        type: 'page',
        is_admin: 1,
        is_member: 0,
        photo_50: 'https://pp.userap...724/2yuKzsqVsfM.jpg',
        photo_100: 'https://pp.userap...723/_6e5V_EBC6c.jpg',
        photo_200: 'https://pp.userap...722/I8vIyaGj5j4.jpg',
      }, {
        id: 24946565,
        name: 'Илья Варламов',
        screen_name: 'varlamov',
        is_closed: 0,
        type: 'page',
        is_admin: 1,
        is_member: 0,
        photo_50: 'https://pp.userap...6af/sjJQdwwYhhw.jpg',
        photo_100: 'https://pp.userap...6ae/1z0PVOr7XIM.jpg',
        photo_200: 'https://pp.userap...6ad/9JcnzjUqaYQ.jpg',
      }],
    },
  })
  .persist();
