const Groups = require('./src/groups');
const User = require('./src/user');

module.exports = {
  Groups,
  User,
};
