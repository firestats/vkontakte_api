# VK API

### Test

`npm test`

### Groups
#### `Groups.Search(access_token, search_word)`

Поиск по группам.

- `access_token (String)` - VK Access Token
- `search_word (String)` - ID, короткое имя или полное имя группы

Возвращает список групп, представленных в виде объекта [группы](https://vk.com/dev/objects/group).

#### `Groups.get_by_user_id(access_token, user_id)`

Алиас [#User.Groups.all](#UserGroupsallaccess_token_user_id_22)

### User

#### `User.Groups.all(access_token, user_id)`

Получение групп пользователя.

**Лимит - 1000 групп!**

- `access_token (String)` - VK Access Token
- `user_id (Number)` - ID пользователя
- `filter (Object)` - Объект вида `{ is_admin: Boolean }`

Возвращает список групп, представленных в виде объекта [группы](https://vk.com/dev/objects/group).